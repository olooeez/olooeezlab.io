export const skillList = [
  {
    title: 'Linguagens de Programação',
    skills: ['C', 'C++', 'Python', 'JavaScript', 'PHP', 'Go'],
  },
  {
    title: 'Frameworks',
    skills: ['React', 'Symfony', 'Laravel', 'Gin', 'Flask', 'OpenCV', 'Numpy'],
  },
  {
    title: 'Banco de dados',
    skills: ['MySQL', 'PostgreSQL', 'SQLite'],
  },
  {
    title: 'Outros',
    skills: ['Git', 'Docker', 'Docker Compose', 'Linux', 'CI/CD'],
  },
];
