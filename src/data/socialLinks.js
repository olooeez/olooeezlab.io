import { FaGitlab, FaMastodon, FaLinkedin, FaReddit } from 'react-icons/fa6';

export const socialLinks = [
  {
    name: 'GitLab',
    url: 'https://gitlab.com/olooeez',
    icon: <FaGitlab scale={1} size={25} />,
  },
  {
    name: 'Mastodon',
    url: 'https://fosstodon.org/@olooeez',
    icon: <FaMastodon scale={1} size={25} />,
  },
  {
    name: 'LinkedIn',
    url: 'https://linkedin.com/in/olooeez',
    icon: <FaLinkedin scale={1} size={25} />,
  },
  {
    name: 'Reddit',
    url: 'https://reddit.com/u/olooeez',
    icon: <FaReddit scale={1} size={25} />,
  },
];
