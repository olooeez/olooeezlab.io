export const siteSections = [
  {
    name: 'Sobre',
    href: 'about',
  },
  {
    name: 'Habilidades',
    href: 'skills',
  },
  {
    name: 'Projetos',
    href: 'projects',
  },
  {
    name: 'Contato',
    href: 'contact',
  },
];
