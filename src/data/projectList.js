export const projectList = [
  {
    id: 1,
    title: 'Budget Alchemy',
    demoLink: 'https://budget-alchemy.netlify.app',
    codeLink: 'https://gitlab.com/olooeez/budget-alchemy',
    description:
      'Plataforma online dedicada ao gerenciamento abrangente de finanças pessoais, oferecendo recursos para monitoramento de despesas e orçamento.',
    tags: ['React', 'Material UI'],
  },
  {
    id: 2,
    title: 'Stay Fit',
    demoLink: 'https://stay-fit-reactjs.netlify.app/',
    codeLink: 'https://gitlab.com/olooeez/stay-fit',
    description:
      'Plataforma online destinada ao treinamento, visualização e exploração de exercícios físicos, permitindo a busca por nome ou parte específica do corpo a ser trabalhada.',
    tags: ['React', 'Material UI', 'RapidAPI'],
  },
  {
    id: 3,
    title: 'Snapcorrect',
    codeLink: 'https://gitlab.com/ufv-projects/hackathon-2023',
    demoLink: null,
    description:
      'Site de correção de provas por marcação, 2º lugar na hackathon 2023 de visão computacional pela Pix Force. Gera relatório de acertos e erros em uma plataforma para o professor.',
    tags: ['Flask', 'Tailwindcss', 'OpenCV', 'Numpy'],
  },
  {
    id: 4,
    title: 'Video Vault',
    codeLink: 'https://gitlab.com/olooeez/video-vault',
    demoLink: null,
    description:
      'REST API de uma plataforma para armazenar e gerenciar seus videos favoritos, com autenticação e autorização.',
    tags: ['Go', 'Gin', 'Gorm', 'PostgreSQL']
  },
  {
    id: 5,
    title: 'Eventando',
    codeLink: 'https://gitlab.com/ufv-projects/eventando',
    demoLink: null,
    description:
      'Plataforma para gerenciar, criar, participar e avaliar eventos da comunidade, com autenticação e autorização.',
    tags: ['PHP', 'MySQL', 'Bootstrap']
  },
  {
    id: 6,
    title: 'blink-sleep-detection',
    codeLink: 'https://gitlab.com/olooeez/blink-sleep-detection',
    demoLink: null,
    description:
      'Projeto desenvolvido para identificar a sonolência de uma pessoa. Este projeto monitora e reconhecer sinais de sonolência em tempo real.',
    tags: ['OpenCV', 'Numpy', 'Mediapipe']
  }
]
