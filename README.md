# olooeez.gitlab.io

My personal website (developer portofolio) made in React with Tailwindcss.

## Demo

![Demo of olooeez.gitlab.io](https://gitlab.com/olooeez/olooeez.gitlab.io/-/raw/main/img/demo.png)

You can find the website live by clicking [here](https://olooeez.gitlab.io).

## Prerequisites

Before getting started, make sure you have the following requirements installed on your machine:

1. **Node.js**: Node.js is a platform that allows the execution of JavaScript code on the server. You can download Node.js from the [official website](https://nodejs.org/) and follow the installation instructions for your operating system.

2. **NPM**: NPM is the package manager for Node.js. It is installed along with Node.js and is necessary for installing project dependencies.

## Installation and Development

### Step 1: Clone the repository

Clone this repository to your desired folder on your machine:

```
git clone https://gitlab.com/olooeez/olooeez.gitlab.io.git
```

### Step 2: Install dependencies

Navigate to the project directory and install the dependencies using NPM:

```
cd olooeez.gitlab.io
npm install
```

### Step 3: Run the application

Start the application locally with the following command:

```
npm run start
```

The application will be available in your browser at [localhost:3000](http://localhost:3000).

**OBS**: Create and edit the `.env` file. You can find a sample by clicking [here](https://gitlab.com/olooeez/olooeez.gitlab.io/-/raw/main/.env.sample).

## Contributing

If you wish to contribute to this project, feel free to open a merge request. We welcome all forms of contribution!

## License

This project is licensed under the [MIT License](https://gitlab.com/olooeez/olooeez.gitlab.io/-/blob/main/LICENSE). Refer to the LICENSE file for more details.
